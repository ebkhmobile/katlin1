package ir.ebkh.sana.main;

public class Person {
    private String firsname;
    private String lastname;
    private String melikod;
    private String fathername;
    private String gender;
    private String tavaloddate;
    private String tavalodcity;
    private String tahsilat;
    private String taahol;
    private String job;
    private String mobile;

    public void setFirsname(String firsname) {
        this.firsname = firsname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setMelikod(String melikod) {
        this.melikod = melikod;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setTavaloddate(String tavaloddate) {
        this.tavaloddate = tavaloddate;
    }

    public void setTavalodcity(String tavalodcity) {
        this.tavalodcity = tavalodcity;
    }

    public void setTahsilat(String tahsilat) {
        this.tahsilat = tahsilat;
    }

    public void setTaahol(String taahol) {
        this.taahol = taahol;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }



    public String getFirsname() {
        return firsname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getMelikod() {
        return melikod;
    }

    public String getFathername() {
        return fathername;
    }

    public String getGender() {
        return gender;
    }

    public String getTavaloddate() {
        return tavaloddate;
    }

    public String getTavalodcity() {
        return tavalodcity;
    }

    public String getTahsilat() {
        return tahsilat;
    }

    public String getTaahol() {
        return taahol;
    }

    public String getJob() {
        return job;
    }

    public String getMobile() {
        return mobile;
    }
}
